//
//  MADPStartingViewController.m
//  MADP
//
//  Created by Kaarel Kruus on 26/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPStartingViewController.h"
#import "MADPConnectionManager.h"
#import "MADPFileSelectionViewController.h"
#import "MADPSessionSelectionViewController.h"
#import "MADPMasterSession.h"
#import "MADPSlaveSession.h"
#import "MADPAppDelegate.h"

@implementation MADPStartingViewController

- (void)reachabilityStatusChanged:(NSNotification *)notification {
    [self updateOnStatus];
}

- (void)updateOnStatus {
    if([[MADPConnectionManager sharedManager] isWifiReachable]) {
        self.noConnectionView.hidden = YES;
    } else {
        self.noConnectionView.hidden = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateOnStatus];
}

- (id)init {
    self = [super initWithNibName:MADPNibName(@"MADPStartingViewController")
                           bundle:[NSBundle mainBundle]];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityStatusChanged:)
                                                     name:MADPConnectionManagerReachabilityChangedNotification
                                                   object:nil];
    }
    return self;
}

- (IBAction)onMaster:(id)sender {
    
    MADPMasterSession *masterSession = [[MADPMasterSession alloc] init];
    [MADPAppDelegate sharedAppDelegate].session = masterSession;
    
    MADPFileSelectionViewController *controller = [[MADPFileSelectionViewController alloc] init];
    //[self presentViewController:controller animated:YES completion:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)onSlave:(id)sender {
    
    MADPSlaveSession *slaveSession = [[MADPSlaveSession alloc] init];
    [MADPAppDelegate sharedAppDelegate].session = slaveSession;
    
    MADPSessionSelectionViewController *controller = [[MADPSessionSelectionViewController alloc] init];
    //[self presentViewController:controller animated:YES completion:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
