//
//  MADPDrawingView.h
//  MADP
//
//  Created by Kaarel Kruus on 25/11/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Common.h"

@interface MADPDrawingView : UIView

@property (nonatomic, copy) MADPDrawBlock drawBlock;
@property (nonatomic, copy) MADPDrawTextFieldBlock drawTextFieldBlock;
@property (nonatomic, copy) MADPMoveBlock moveBlock;

- (UIView *)viewForPoint:(CGPoint)point;
- (UIView *)viewForGuid:(NSString *)guid;

@end
