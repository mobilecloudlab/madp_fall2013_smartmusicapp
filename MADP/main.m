//
//  main.m
//  MADP
//
//  Created by Kaarel Kruus on 26/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MADPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MADPAppDelegate class]));
    }
}
