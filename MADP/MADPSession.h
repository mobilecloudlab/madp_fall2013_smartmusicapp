//
//  MADPSession.h
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPObject.h"

/*
@protocol MADPSessionDelegate

- (void)onEventReceived:(NSString *)event fromOriginator:(NSString *)originator;
- (void)sessionTerminated:(id)session forReason:(NSString *)reason;

@end
 
*/


@interface MADPSession : MADPObject

/*
@property (nonatomic, weak) id<MADPSessionDelegate> delegate;

- (BOOL)start;
- (void)stop;
*/
 
@end
