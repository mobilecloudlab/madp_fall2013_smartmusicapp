//
//  MADPMasterSession.m
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPMasterSession.h"

@interface MADPMasterSession ()

/*
@property (nonatomic, strong) Server *server;
@property (nonatomic, strong) NSMutableSet *clients;
*/

@end

@implementation MADPMasterSession

/*
- (BOOL)start {
    self.server = [[Server alloc] init];
    self.server.delegate = self;
    if (![self.server start]) {
        self.server = nil;
        return NO;
    }
    return YES;
}

- (void)stop {
    [self.server stop];
    self.server = nil;
    [self.clients makeObjectsPerformSelector:@selector(close)];
}

- (id)init {
    self = [super init];
    if(self) {
        self.clients = [[NSMutableSet alloc] init];
    }
    return self;
}

- (void)dealloc {
    self.server = nil;
    self.clients = nil;
}

- (void)sendEvent:(NSString *)event fromOriginator:(NSString *)originator {
    NSDictionary *packet = @{@"event":event, @"originator":originator};
    [self.clients makeObjectsPerformSelector:@selector(sendNetworkPacket:) withObject:packet];
}

- (void) serverFailed:(Server *)server reason:(NSString*)reason {
    [self stop];
    [self.delegate sessionTerminated:self forReason:reason];
}

- (void) handleNewConnection:(Connection *)connection {
    connection.delegate = self;
    [self.clients addObject:connection];
    NSLog(@"New Connection: %i", self.clients.count);
}

- (void) connectionAttemptFailed:(Connection *)connection { }

- (void) connectionTerminated:(Connection *)connection {
    [self.clients removeObject:connection];
    NSLog(@"Connection Lost: %i", self.clients.count);
}

- (void)receivedNetworkPacket:(NSDictionary *)packet viaConnection:(Connection *)connection {
    // TODO: received something from a slave, distribute to all other slaves as well!
}
 */

@end
