//
//  Score+Additions.m
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "Score+Additions.h"

@implementation Score (Additions)

+ (NSString *)entityName {
    return @"Score";
}

@end
