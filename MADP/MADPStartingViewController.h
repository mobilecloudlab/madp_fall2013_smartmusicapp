//
//  MADPStartingViewController.h
//  MADP
//
//  Created by Kaarel Kruus on 26/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPViewController.h"

@interface MADPStartingViewController : MADPViewController

@property (nonatomic, weak) IBOutlet UIView *noConnectionView;

- (IBAction)onMaster:(id)sender;
- (IBAction)onSlave:(id)sender;

@end
