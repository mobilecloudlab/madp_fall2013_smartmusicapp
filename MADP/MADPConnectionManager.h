//
//  MADPConnectionManager.h
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPObject.h"
#import "Reachability.h"

extern NSString *MADPConnectionManagerReachabilityChangedNotification;

@interface MADPConnectionManager : MADPObject

+ (id)sharedManager;

- (BOOL)isWifiReachable;

@end
