//
//  MADPScoreViewController.h
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPViewController.h"
#import "MWPhotoBrowser.h"
#import "MADPSession.h"
#import "Common.h"
#import "MADPDrawingView.h"

@interface MADPScoreViewController : MADPViewController <MWPhotoBrowserDelegate>

@property (nonatomic, copy) MADPDrawBlock drawBlock;
@property (nonatomic, copy) MADPDrawTextFieldBlock drawTextFieldBlock;
@property (nonatomic, copy) MADPMoveBlock moveBlock;

@property (nonatomic, strong) MADPDrawingView *drawingView;
@property (nonatomic, copy) MADPTurnPageBlock turnPageBlock;
@property (nonatomic, strong) MWPhotoBrowser *browser;

- (id)initWithChecksum:(NSString *)checksum;

@end
