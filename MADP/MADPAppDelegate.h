//
//  MADPAppDelegate.h
//  MADP
//
//  Created by Kaarel Kruus on 26/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MADPSession.h"

@interface MADPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) MADPSession *session;
@property (strong, nonatomic) UIWindow *window;

+ (MADPAppDelegate *)sharedAppDelegate;

@end
