//
//  MADPFileSelectionViewController.m
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPFileSelectionViewController.h"
#import "MADPDataManager.h"
#import "Score+Additions.h"
#import "MADPScoreViewController.h"
#import "MADPMasterSession.h"
#import "MADPScoreViewController.h"
#import "MADPPassCodeViewController.h"
#import "MADPAppDelegate.h"
#import "MBProgressHUD.h"

#import "TBXML.h"
#import "TBXML+HTTP.h"

#import "MADPFileSet.h"
#import "MADPConstants.h"

#import "AFNetworking.h"
#import "MADPFileManager.h"
#import "SSZipArchive.h"
#import "MADPFileManager.h"

@interface MADPFileSelectionViewController ()

@property (nonatomic, strong) NSMutableArray *downloadedFileSets; // all filesets downloaded to the device
@property (nonatomic, strong) NSMutableArray *availableFileSets; // all filesets available via the service
@property (nonatomic, strong) NSMutableArray *freshFileSets; // all filesets available, but not downlaoded

@property (nonatomic, copy) MADPBlock invitationBlock;

@property (nonatomic, strong) MADPFileSet *selectedFileSet;

@end

@implementation MADPFileSelectionViewController

#define DOWNLOADED_FILESETS_SECTION 0
#define FRESH_FILESETS_SECTION 1

- (void)loadAvailableFileSets:(MADPBlock)completionBlock {
    
    // clear current array
    self.availableFileSets = [[NSMutableArray alloc] init];
    
    NSString *serviceUrl = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"DefaultServiceUrl"];
    
    TBXML *tbxml = [[TBXML alloc] initWithURL:[NSURL URLWithString:serviceUrl] success:^(TBXML *tbxml) {
        
        TBXMLElement *root = tbxml.rootXMLElement;
        
        TBXMLElement *fileset = [TBXML childElementNamed:@"fileset" parentElement:root];
        
        while (fileset != nil) {
            
            MADPFileSet *f = [[MADPFileSet alloc] init];
            f.title = [TBXML valueOfAttributeNamed:@"title" forElement:fileset];
            f.composer = [TBXML valueOfAttributeNamed:@"composer" forElement:fileset];
            f.lyrics = [TBXML valueOfAttributeNamed:@"lyrics" forElement:fileset];
            f.href = [TBXML valueOfAttributeNamed:@"href" forElement:fileset];
            f.checksum = [TBXML valueOfAttributeNamed:@"checksum" forElement:fileset];
            f.origin = [TBXML valueOfAttributeNamed:@"origin" forElement:fileset];
            
            [self.availableFileSets addObject:f];
            
            fileset = [TBXML nextSiblingNamed:@"fileset" searchFromElement:fileset];
        }
        
        if(completionBlock) {
            completionBlock();
        }
        
    } failure:^(TBXML *tbxml, NSError *error) {
        
        if(completionBlock) {
            completionBlock();
        }
    }];
}

- (void)loadDownloadedFileSets {
    
    self.downloadedFileSets = [[NSMutableArray alloc] init];
    
    NSError *error;
    
    NSArray *downloadedFileSetDirs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[MADPFileManager sharedManager] fileSetsPath] error:&error];
    
    for (NSString *checksum in downloadedFileSetDirs) {
        
        MADPFileSet *fileSet = [[MADPFileSet alloc] init];
        fileSet.title = checksum;
        fileSet.checksum = checksum;
        
        [self.downloadedFileSets addObject:fileSet];
    }
    
}

- (void)filterFreshFileSets {
    
    self.freshFileSets = [[NSMutableArray alloc] init];
    
    // all the filesets not being downloaded and being available are considered as "fresh"
    for (MADPFileSet *availableFileSet in self.availableFileSets) {
        if(![self isFileSetDownloaded:availableFileSet]) {
            [self.freshFileSets addObject:availableFileSet];
        }
    }
}

- (BOOL)isFileSetDownloaded:(MADPFileSet *)fileSet {
    for (MADPFileSet *downloadedFileSet in self.downloadedFileSets) {
        if([fileSet.checksum isEqualToString:downloadedFileSet.checksum]) {
            return YES;
        }
    }
    return NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.serviceAdvertiser) {
        NSLog(@"STOP AD!");
        [self.serviceAdvertiser stopAdvertisingPeer];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en"]];
    [dateFormat setDateFormat: @"dd.MM.yyyy HH:mm:ss.SSSSSS ZZZ"];
    NSString *currentTimestamp = [dateFormat stringFromDate:[NSDate date]];
    
    MCPeerID *peerID = [[MCPeerID alloc] initWithDisplayName:[NSString stringWithFormat:@"Conductor (%@)", currentTimestamp]];
    self.session = [[MCSession alloc] initWithPeer:peerID];
    self.session.delegate = self;
    
    self.serviceAdvertiser = [[MCNearbyServiceAdvertiser alloc] initWithPeer:peerID
                                                               discoveryInfo:nil
                                                                 serviceType:@"partiture"];
    self.serviceAdvertiser.delegate = self;
    [self.serviceAdvertiser stopAdvertisingPeer];
    
    /* show the filesets already downloaded to the device */
    
    
    /* (try to) download the list again (maybe there are new filesets) */
    
    
    /*
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Syncing...";
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [NSThread sleepForTimeInterval:5.0F];
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
    */
    
    [self refreshFileSets];
}

- (void)refreshFileSets {
    
    [self loadAvailableFileSets:^{
        [self loadDownloadedFileSets];
        [self filterFreshFileSets];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.dataTableView reloadData];
        });
    }];
    
}

- (id)init {
    self = [super initWithNibName:MADPNibName(@"MADPFileSelectionViewController")
                           bundle:[NSBundle mainBundle]];
    if(self) {
        
        //self.downloadedFileSets = [[NSMutableArray alloc] init];
        //self.availableFileSets = [[NSMutableArray alloc] init];
        //self.freshFileSets = [[NSMutableArray alloc] init];
        
        /*
        NSArray *scores = [[MADPDataManager sharedManager] getScores];
        
        if(!scores || scores.count == 0) {
            
            // TODO: remove dummy data pushing to DB
            NSManagedObjectContext *context = [[MADPDataManager sharedManager] managedObjectContext];
            Score *score = [NSEntityDescription insertNewObjectForEntityForName:[Score entityName]
                                          inManagedObjectContext:context];
            score.title = @"A Thousand miles";
            score.composer = @"Vanessa Carlton";
            score.lyrics = @"Vanessa Carlton";
            NSError *error = nil;
            [context save:&error];
        }
        
        self.scores = [[MADPDataManager sharedManager] getScores];
        */
    }
    return self;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.section == DOWNLOADED_FILESETS_SECTION);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == DOWNLOADED_FILESETS_SECTION) {
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            NSString *checksum = [[self.downloadedFileSets objectAtIndex:indexPath.row] checksum];
            
            NSString *path = [[MADPFileManager sharedManager] fileSetsPath];
            
            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", path, checksum]
                                                       error:nil];
            
            [self refreshFileSets];
        }
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.dataTableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.dataTableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0F;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return (section == DOWNLOADED_FILESETS_SECTION) ? @"Downloaded scores" : @"New scores";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (section == DOWNLOADED_FILESETS_SECTION) ? self.downloadedFileSets.count : self.freshFileSets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = @"fileSetListCell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }
    
    if(indexPath.section == DOWNLOADED_FILESETS_SECTION) {
        
        MADPFileSet *fileset = [self.downloadedFileSets objectAtIndex:indexPath.row];
        cell.textLabel.text = fileset.title;
        cell.detailTextLabel.text = fileset.composer;
        
        if([self string:fileset.title containsString:self.searchBar.text] ||
           [self string:fileset.composer containsString:self.searchBar.text]) {
            
            cell.backgroundColor = [UIColor yellowColor];
            
        } else {
            cell.backgroundColor = [UIColor clearColor];
        }
        
    } else {
        
        MADPFileSet *fileset = [self.freshFileSets objectAtIndex:indexPath.row];
        cell.textLabel.text = fileset.title;
        cell.detailTextLabel.text = fileset.composer;
        
        if([self string:fileset.title containsString:self.searchBar.text] ||
           [self string:fileset.composer containsString:self.searchBar.text]) {
         
            cell.backgroundColor = [UIColor yellowColor];
            
        } else {
            cell.backgroundColor = [UIColor clearColor];
        }
    }
    
    
    /*
    Score *score = self.scores[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ (Music: %@, Lyrics: %@)",
                           score.title, score.composer, score.lyrics];
    */
    
    return cell;
}

- (BOOL)string:(NSString *)str1 containsString:(NSString *)str2 {
    return (([str1 rangeOfString:str2].location != NSNotFound) && str1 && str2);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
    MADPMasterSession *masterSession = [[MADPMasterSession alloc] init];
    MADPScoreViewController *controller = [[MADPScoreViewController alloc] initWithSession:masterSession];
    [self presentViewController:controller animated:YES completion:nil];
     */
    
    if(indexPath.section == DOWNLOADED_FILESETS_SECTION) {
        
        MADPPassCodeViewController *passCodeViewController = [[MADPPassCodeViewController alloc] init];
        [passCodeViewController setCompletionBlock:^(NSString *passCode) {
            
            MADPMasterSession *masterSession = [[MADPMasterSession alloc] init];
            masterSession.passCode = passCode;
            [MADPAppDelegate sharedAppDelegate].session = masterSession;
            
            NSLog(@"START AD!");
            [self.serviceAdvertiser startAdvertisingPeer];
            
            MADPFileSet *fileSet = [self.downloadedFileSets objectAtIndex:indexPath.row];
            self.selectedFileSet = fileSet;
            
            MADPScoreViewController *controller = [[MADPScoreViewController alloc] initWithChecksum:fileSet.checksum];
            [controller setDrawBlock:^(CGRect rect, NSString *guid) {
                
                // send it to all slaves!
                
                NSString *rectString = NSStringFromCGRect(rect);
                NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                                   rectString, @"rect",
                                   guid, @"guid",
                                   nil];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:d];
                if(self.clientPeer) {
                    [self.session sendData:data toPeers:[NSArray arrayWithObject:self.clientPeer] withMode:MCSessionSendDataReliable error:nil];
                }
                
            }];
            [controller setDrawTextFieldBlock:^(UITextField *textField, NSString *guid) {
                
                // send it to all slaves!
                NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                                   textField, @"textField",
                                   guid, @"guid",
                                   nil];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:d];
                if(self.clientPeer) {
                    [self.session sendData:data toPeers:[NSArray arrayWithObject:self.clientPeer] withMode:MCSessionSendDataReliable error:nil];
                }
            }];
            [controller setTurnPageBlock:^(NSInteger toPage) {
                
                // send it to all slaves!
                NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:@(toPage), @"pageTurn", nil];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:d];
                if(self.clientPeer) {
                    [self.session sendData:data toPeers:[NSArray arrayWithObject:self.clientPeer] withMode:MCSessionSendDataReliable error:nil];
                }
            }];
            
            [controller setMoveBlock:^(CGPoint point, NSString *guid) {
                
                NSLog(@"SEND MOVE!");
                
                // send it to all slaves!
                NSString *pointString = NSStringFromCGPoint(point);
                NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                                   pointString, @"point",
                                   guid, @"guid",
                                   nil];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:d];
                if(self.clientPeer) {
                    [self.session sendData:data toPeers:[NSArray arrayWithObject:self.clientPeer] withMode:MCSessionSendDataReliable error:nil];
                }
            }];
            
            //[self presentViewController:controller animated:YES completion:nil];
            [self.navigationController pushViewController:controller animated:YES];
            
        }];
        //[self presentViewController:passCodeViewController animated:YES completion:nil];
        [self.navigationController pushViewController:passCodeViewController animated:YES];
        
    } else {
        
        /* trigger download */
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        MADPFileManager *fileManager = [MADPFileManager sharedManager];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        
        MADPFileSet *fileSet = [self.freshFileSets objectAtIndex:indexPath.row];
        
        NSURL *URL = [NSURL URLWithString:[fileSet href]];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        
        NSString *fileSetsDirPath = [fileManager fileSetsPath];
        NSString *zipFileName = [NSString stringWithFormat:@"%@.zip", [fileSet checksum]];
        NSString *zipFilePath = [fileSetsDirPath stringByAppendingFormat:@"/%@", zipFileName];
        NSString *unzipDirPath = [fileSetsDirPath stringByAppendingFormat:@"/%@", [fileSet checksum]];
        
        NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request
                                                                         progress:nil
                                                                      destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
            return [NSURL fileURLWithPath:zipFilePath];
            
        } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            
            /* unzip */
            NSString *input = zipFilePath;
            NSString *output = unzipDirPath;
            
            NSError *err;
            [[NSFileManager defaultManager] createDirectoryAtPath:output
                                      withIntermediateDirectories:NO
                                                       attributes:nil
                                                             error:&err];
            
            [SSZipArchive unzipFileAtPath:input toDestination:output];
            
            /* delete zip file */
            [[[NSFileManager alloc] init] removeItemAtPath:input error:nil];
    
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hide:YES];
                [self refreshFileSets];
            });
        }];
        
        [manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
            
            double d_percentage = ((double)totalBytesWritten / (double)totalBytesExpectedToWrite);
            int i_percentage = (int)(d_percentage * 100);
            
            hud.labelText = [NSString stringWithFormat:@"Downloading...%i%%", i_percentage];
            
        }];
        
        [downloadTask resume];
    }
}

- (void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didReceiveInvitationFromPeer:(MCPeerID *)peerID
       withContext:(NSData *)context
 invitationHandler:(void (^)(BOOL, MCSession *))invitationHandler {
    
    NSLog(@"- - - didReceiveInvitationFromPeer: (%@)", peerID.displayName);
    
    self.invitationBlock = ^() {
        invitationHandler(YES, self.session);
    };
    
    // automatically reject apps with wrong passcode
    NSString *passCode = [[NSString alloc] initWithData:context encoding:NSUTF8StringEncoding];
    
    MADPMasterSession *masterSession = (MADPMasterSession *)[MADPAppDelegate sharedAppDelegate].session;
    
    if([passCode isEqualToString:masterSession.passCode]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Join Request"
                                                        message:[NSString stringWithFormat:@"Accept %@", peerID.displayName]
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
        [alert show];
        
    } else {
        NSLog(@"PASSCODES ARE NOT EQUAL, REJECT");
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1) { // YES
        
        if(self.invitationBlock) {
            self.invitationBlock();
        }
    }
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID {
    NSLog(@"- - - didReceiveData");
    
    //NSString *request = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //if([request isEqualToString:@"sendMe"]) {
        // send the resource to the peer
        
    //}
}

- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID {
    NSLog(@"- - - didReceiveStream");
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress {
    NSLog(@"- - - didStartReceivingResourceWithName");
}

- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error {
    NSLog(@"- - - didFinishReceivingResourceWithName");
}

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state {
    NSLog(@"- - - didChangeState");
    
    if(state == MCSessionStateConnected) {
        
        self.clientPeer = peerID;
        
        // send it the file
        /*
        NSString *file = [NSString stringWithFormat:@"%@/%@.zip",
                          [[MADPFileManager sharedManager] fileSetsPath],
                          self.selectedFileSet.checksum];
        
        NSURL *filesetsPathURL = [[NSURL alloc] initFileURLWithPath:file];
        //filesetsPathURL = [filesetsPathURL URLByAppendingPathComponent:filename];
        
        NSLog(@"Send at: %@", filesetsPathURL.absoluteString);
        
        [self.session sendResourceAtURL:filesetsPathURL
                               withName:self.selectedFileSet.checksum
                                 toPeer:peerID
                  withCompletionHandler:^(NSError *error) {
                      
                      NSLog(@"COMP (err): %@", error);
        }];
        */
        
        // distribute the url instead
        
        NSDictionary *d = [NSDictionary dictionaryWithObjectsAndKeys:
                           self.selectedFileSet.checksum, @"fileSetChecksum",
                           @"http://ut.ee/~a72093/public/2013/partiture/filesets/75cfff70658e027729d80478b7170b57.zip", @"fileSetHref",
                           nil];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:d];
        if(self.clientPeer) {
            [self.session sendData:data toPeers:[NSArray arrayWithObject:self.clientPeer] withMode:MCSessionSendDataReliable error:nil];
        }
        
    } else {
        self.clientPeer = nil;
    }
}

@end
