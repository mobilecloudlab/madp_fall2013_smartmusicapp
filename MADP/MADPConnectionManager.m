//
//  MADPConnectionManager.m
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPConnectionManager.h"

NSString *MADPConnectionManagerReachabilityChangedNotification = @"MADPConnectionManagerReachabilityChangedNotification";

@interface MADPConnectionManager ()

@property (nonatomic, strong) Reachability *reachability;

@end

@implementation MADPConnectionManager

static MADPConnectionManager *sharedManager = nil;

- (BOOL)isWifiReachable {
    NetworkStatus status = [self.reachability currentReachabilityStatus];
    return (status == ReachableViaWiFi);
}

+ (id)sharedManager {
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedManager = [[MADPConnectionManager alloc] init];
    });
    return sharedManager;
}

- (void)reachabilityChanged:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] postNotificationName:MADPConnectionManagerReachabilityChangedNotification object:nil userInfo:nil];
}

- (id)init {
    self = [super init];
    if(self) {
        self->_reachability = [Reachability reachabilityForLocalWiFi];
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        
        [self.reachability startNotifier];
    }
    return self;
}

- (void)dealloc {
    [self.reachability stopNotifier];
    self->_reachability = nil;
}

@end
