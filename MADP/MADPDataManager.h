//
//  MADPDataManager.h
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPObject.h"

@interface MADPDataManager : MADPObject

+ (id)sharedManager;

- (NSArray *)getScores;

@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;

@end
