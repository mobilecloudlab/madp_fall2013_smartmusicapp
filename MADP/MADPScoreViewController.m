//
//  MADPScoreViewController.m
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPScoreViewController.h"
#import "MADPMasterSession.h"
#import "MADPDrawingView.h"
#import "MADPFileManager.h"
#import "MADPAppDelegate.h"

@interface MADPScoreViewController ()

@property (nonatomic, strong) NSString *checksum;

@property (nonatomic) BOOL isEditMode;
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic, strong) MADPSession *session;

@property (nonatomic, strong) NSMutableArray *drawingViews;

@end

@implementation MADPScoreViewController

- (id)initWithChecksum:(NSString *)checksum {
    self = [super init];
    if (self) {
        self.drawingViews = [[NSMutableArray alloc] init];
        self.checksum = checksum;
        [self loadPhotos];
    }
    return self;
}

- (id)init {
    return [self initWithChecksum:nil];
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    if([[MADPAppDelegate sharedAppDelegate].session isKindOfClass:[MADPMasterSession class]]) {
        
        //NSLog(@"SEND PAGE CHAGNE!");
        
        for(UIView *subview in self.browser.view.subviews) {
            if([subview isKindOfClass:[MADPDrawingView class]]) {
                for(UIView *_subview in subview.subviews) {
                    [_subview removeFromSuperview];
                }
            }
        }
        
        if(self.turnPageBlock) {
            self.turnPageBlock(index);
        }
        
        
        
        // ONLY FOR MASTER!
        //NSString *indexStr = [NSString stringWithFormat:@"%i", index];
        //[(MADPMasterSession *)self.session sendEvent:indexStr fromOriginator:@""];
    }
}

/*
- (void)onEventReceived:(NSString *)event fromOriginator:(NSString *)originator {
    if(![self.session isKindOfClass:[MADPMasterSession class]]) {
        // ONLY FOR SLAVE!
        NSInteger index = [event integerValue];
        [self.browser setCurrentPhotoIndex:index];
    }
}
 */

/*
- (void)sessionTerminated:(id)session forReason:(NSString *)reason {
    NSLog(@"Session terminated!");
}
*/
/*
- (void)startSession {
    if([self.session start]) {
        NSLog(@"Staring the session was successful");
    } else {
        NSLog(@"Staring the session failed");
    }
}
*/

- (void)loadPhotos {

    NSMutableArray *photos = [NSMutableArray array];
    /*
    for(int i = 1; i <= 8; i++) {
        NSString *index = [NSString stringWithFormat:@"%i", i];
        NSString *imageFile = [[NSBundle mainBundle] pathForResource:index ofType:@"png"];
        UIImage *image = [[UIImage alloc] initWithContentsOfFile:imageFile];
        MWPhoto *photo = [MWPhoto photoWithImage:image];
        [photos addObject:photo];
    }
     */
    
    NSArray *filenames = [[MADPFileManager sharedManager] filePathsForChecksum:self.checksum];
    for (NSString *filename in filenames) {
        //NSLog(@"Filename: %@", filename);
        
        NSString *filepath = [NSString stringWithFormat:@"%@/%@/%@",
                              [[MADPFileManager sharedManager] fileSetsPath], self.checksum, filename];
        
        UIImage *image = [[UIImage alloc] initWithContentsOfFile:filepath];
        MWPhoto *photo = [MWPhoto photoWithImage:image];
        [photos addObject:photo];
        
        // for each photo add a drawing layer
        
        //[self.drawingViews addObject:<#(id)#>]
        
    }
    
    
    
    self.photos = photos;
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    return [self.photos objectAtIndex:index];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(!self.browser) {
        
        self.browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
        
        UIBarButtonItem *editBtn = [[UIBarButtonItem alloc]
                                    initWithTitle:@"Edit"
                                    style:UIBarButtonItemStyleBordered
                                    target:self
                                    action:@selector(onEditBtn:)];
        self.browser.navigationItem.rightBarButtonItem = editBtn;
        
        [self.navigationController pushViewController:self.browser animated:YES];
    } else {
        
        [self.navigationController popViewControllerAnimated:NO];
    }
    
    //[self presentViewController:self.browser animated:YES completion:nil];
    
    // add drawing layer
    self.drawingView = [[MADPDrawingView alloc] initWithFrame:self.browser.view.frame];
    [self.drawingView setDrawBlock:^(CGRect rect, NSString *guid) {
        
        if(self.drawBlock) {
            self.drawBlock(rect, guid);
        }
    }];
    [self.drawingView setDrawTextFieldBlock:^(UITextField *textField, NSString *guid) {
        
        if(self.drawTextFieldBlock) {
            self.drawTextFieldBlock(textField, guid);
        }
    }];
    [self.drawingView setMoveBlock:^(CGPoint point, NSString *guid) {
        
        if(self.moveBlock) {
            self.moveBlock(point, guid);
        }
    }];
    
    self.drawingView.userInteractionEnabled = self.isEditMode;
    [self.browser.view addSubview:self.drawingView];
}

- (void)onEditBtn:(id)sender {
    self.isEditMode = !self.isEditMode;
    self.drawingView.userInteractionEnabled = self.isEditMode;
}

- (void)photoBrowserWillBeginDragging:(MWPhotoBrowser *)photoBrowser {
    for(UIView *subview in self.browser.view.subviews) {
        if([subview isKindOfClass:[MADPDrawingView class]]) {
            for(UIView *_subview in subview.subviews) {
                [_subview removeFromSuperview];
            }
        }
    }
}

@end
