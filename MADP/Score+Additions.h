//
//  Score+Additions.h
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "Score.h"

@interface Score (Additions)

+ (NSString *)entityName;

@end
