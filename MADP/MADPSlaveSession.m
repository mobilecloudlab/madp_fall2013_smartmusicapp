//
//  MADPSlaveSession.m
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPSlaveSession.h"

@interface MADPSlaveSession ()

//@property (nonatomic, strong) Connection *connection;

@end

@implementation MADPSlaveSession

/*
- (id)initWithHost:(NSString *)host andPort:(NSInteger)port {
    self = [super init];
    if(self) {
        self.connection = [[Connection alloc] initWithHostAddress:host andPort:port];
    }
    return self;
}

- (id)initWithNetService:(NSNetService *)netService {
    self = [super init];
    if(self) {
        self.connection = [[Connection alloc] initWithNetService:netService];
    }
    return self;
}

- (void)dealloc {
    self.connection = nil;
}

- (BOOL)start {
    if (self.connection == nil) {
        return NO;
    }
    self.connection.delegate = self;
    return [self.connection connect];
}

- (void)stop {
    if (self.connection == nil) {
        return;
    }
    [self.connection close];
    self.connection = nil;
}

- (void)connectionAttemptFailed:(Connection *)connection {
    [self.delegate sessionTerminated:self forReason:@"Can not connect to the master"];
}

- (void)connectionTerminated:(Connection *)connection {
    [self.delegate sessionTerminated:self forReason:@"Master closed the connection"];
}

- (void)receivedNetworkPacket:(NSDictionary *)packet viaConnection:(Connection *)connection {
    
    NSLog(@"Received Packet!");
    
    NSString *event = packet[@"event"];
    NSString *originator = packet[@"originator"];
    
    [self.delegate onEventReceived:event fromOriginator:originator];
}
 */

@end
