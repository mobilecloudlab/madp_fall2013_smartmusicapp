//
//  MADPTextField.h
//  MADP
//
//  Created by Kaarel Kruus on 16/12/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MADPTextField : UITextField

@property (nonatomic, strong) NSString *guid;

@end
