//
//  Common.h
//  MADP
//
//  Created by Kaarel Kruus on 25/11/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#ifndef MADP_Common_h
#define MADP_Common_h

typedef void (^MADPBlock)(void);
typedef void (^MADPDrawBlock)(CGRect rect);
typedef void (^MADPDrawTextFieldBlock)(UITextField *textField);

#endif
