//
//  MADPFileSet.h
//  MADP
//
//  Created by Kaarel Kruus on 12/12/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPObject.h"

@interface MADPFileSet : MADPObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *composer;
@property (strong, nonatomic) NSString *lyrics;
@property (strong, nonatomic) NSString *href;
@property (strong, nonatomic) NSString *checksum;
@property (strong, nonatomic) NSString *origin;

@end
