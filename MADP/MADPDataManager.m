//
//  MADPDataManager.m
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPDataManager.h"
#import <CoreData/CoreData.h>
#import "Score+Additions.h"

@interface MADPDataManager ()

@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation MADPDataManager

@synthesize managedObjectContext = _managedObjectContext;

static MADPDataManager *sharedManager = nil;

+ (id)sharedManager {
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedManager = [[MADPDataManager alloc] init];
    });
    return sharedManager;
}

- (NSArray *)getScores {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[Score entityName]];
    NSArray *results = [context executeFetchRequest:request error:&error];
    if(error == nil) {
        return results;
    }
    return nil;
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
    if (self->_managedObjectContext != nil) {
        return self->_managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        self->_managedObjectContext = [[NSManagedObjectContext alloc] init];
        [self->_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return self->_managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (self->_managedObjectModel != nil) {
        return self->_managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MADPDataModel" withExtension:@"momd"];
    self->_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return self->_managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (self->_persistentStoreCoordinator != nil) {
        return self->_persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MADPDataModel.sqlite"];
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    self->_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![self->_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return self->_persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

@end
