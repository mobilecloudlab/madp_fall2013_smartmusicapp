//
//  MADPPassCodeViewController.m
//  MADP
//
//  Created by Kaarel Kruus on 24/11/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPPassCodeViewController.h"
#import "MADPAppDelegate.h"
#import "MADPScoreViewController.h"

#import "MADPMasterSession.h"
#import "MADPSlaveSession.h"

@interface MADPPassCodeViewController ()

@end

@implementation MADPPassCodeViewController

- (id)init {
    self = [super initWithNibName:MADPNibName(@"MADPPassCodeViewController")
                           bundle:[NSBundle mainBundle]];
    if (self) {
        
    }
    return self;
}

- (IBAction)onEnter:(id)sender {
    
    id session = [[MADPAppDelegate sharedAppDelegate] session];
    
    if([session isKindOfClass:[MADPMasterSession class]]) { // STORE PASSCODE FOR MASTER
        
        [self.navigationController popViewControllerAnimated:NO];
        
        if(self.completionBlock) {
            self.completionBlock(self.passCodeTextField.text);
        }
        
        /*
        [self dismissViewControllerAnimated:YES completion:^{
            if(self.completionBlock) {
                self.completionBlock(self.passCodeTextField.text);
            }
        }];
         */
        
    }
    else if([session isKindOfClass:[MADPSlaveSession class]]) { // POST PASSCODE FOR MASTER
        
        [self.navigationController popViewControllerAnimated:NO];
        
        if(self.completionBlock) {
            self.completionBlock(self.passCodeTextField.text);
        }
        
        /*
        [self dismissViewControllerAnimated:YES completion:^{
            if(self.completionBlock) {
                self.completionBlock(self.passCodeTextField.text);
            }
        }];
         */
    }
}

@end
