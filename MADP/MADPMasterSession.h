//
//  MADPMasterSession.h
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPSession.h"
/*
#import "Server.h"
#import "ServerDelegate.h"
#import "Connection.h"
#import "ConnectionDelegate.h"
*/

@interface MADPMasterSession : MADPSession //<ServerDelegate, ConnectionDelegate>

//- (void)sendEvent:(NSString *)event fromOriginator:(NSString *)originator;

@property (nonatomic, strong) NSString *passCode;

@end
