//
//  MADPViewController.m
//  MADP
//
//  Created by Kaarel Kruus on 26/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPViewController.h"

@implementation MADPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)sharedInit {
    // iOS5 & 6 compatability twist, beacuse iOS7 will by default stretch the view to fullscreen
    //self.wantsFullScreenLayout = YES;
}

- (id)init {
    self = [super init];
    if (self) {
        [self sharedInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self sharedInit];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self) {
        [self sharedInit];
    }
    return self;
}

@end
