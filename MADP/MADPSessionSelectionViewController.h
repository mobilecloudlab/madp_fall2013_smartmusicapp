//
//  MADPSessionSelectionViewController.h
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPViewController.h"
#import "ServerBrowserDelegate.h"
#import <MultipeerConnectivity/MultipeerConnectivity.h>

@interface MADPSessionSelectionViewController : MADPViewController <UITableViewDataSource, UITableViewDelegate, MCNearbyServiceBrowserDelegate, MCSessionDelegate>

@property (nonatomic, strong) MCNearbyServiceBrowser *serviceBrowser;
@property (nonatomic, strong) MCSession *session;

@property (nonatomic, weak) IBOutlet UITableView *dataTableView;

@end
