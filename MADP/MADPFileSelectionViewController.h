//
//  MADPFileSelectionViewController.h
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPViewController.h"
#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import "Common.h"

@interface MADPFileSelectionViewController : MADPViewController <UITableViewDataSource, UITableViewDelegate,MCNearbyServiceAdvertiserDelegate, MCSessionDelegate, UIAlertViewDelegate, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UITableView *dataTableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) MCNearbyServiceAdvertiser *serviceAdvertiser;
@property (nonatomic, strong) MCSession *session;

@property (nonatomic, strong) MCPeerID *clientPeer;


@end
