//
//  MADPDrawingView.m
//  MADP
//
//  Created by Kaarel Kruus on 25/11/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPDrawingView.h"
#import "MADPView.h"
#import "MADPTextField.h"

@interface MADPDrawingView ()

@property (nonatomic) BOOL hasStartingPoint;
@property (nonatomic) CGPoint startingPoint;

@property (nonatomic) BOOL isMoving;

@property (nonatomic, strong) UIView *movingView;

@end

@implementation MADPDrawingView

- (NSString *)generateUUIDString
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //[super touchesBegan:touches withEvent:event];
    
    UITouch *touch = [[event allTouches] anyObject];
    self.startingPoint = [touch locationInView:self];
    self.hasStartingPoint = YES;
    
    CGPoint touchPoint = [touch locationInView:self];
    
    self.movingView = [self viewForPoint:touchPoint];
    
    if(self.movingView) {
        self.isMoving = YES;
    }
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    //[super touchesMoved:touches withEvent:event];
    
    UITouch *touch = [[event allTouches] anyObject];
    //NSLog(@"Touches moved!");
    
    if(self.isMoving) {
        CGPoint endingPoint = [touch locationInView:self];
        
        CGRect frame = self.movingView.frame;
        frame.origin = endingPoint;
        self.movingView.frame = frame;
        
        NSString *guid = @"";
        if([self.movingView respondsToSelector:@selector(guid)]) {
            guid = [self.movingView performSelector:@selector(guid)];
            //NSLog(@"GUID: %@", guid);
        }
        
        // send event!
        if(self.moveBlock) {
            
            self.moveBlock(endingPoint, guid);
        }
        
    }
}

- (UIView *)viewForPoint:(CGPoint)point {
    for(UIView *subview in self.subviews) {
        if([subview isKindOfClass:[MADPView class]] ||
           [subview isKindOfClass:[MADPTextField class]]) {
            if(CGRectContainsPoint(subview.frame, point)) {
                return subview;
            }
        }
    }
    return nil;
}

- (UIView *)viewForGuid:(NSString *)guid {
    for(UIView *subview in self.subviews) {
        if([subview isKindOfClass:[MADPView class]] ||
           [subview isKindOfClass:[MADPTextField class]]) {
            
            NSString *guid = [subview performSelector:@selector(guid)];
            return subview;
            
        }
    }
    return nil;
}

- (void)onView:(UILongPressGestureRecognizer *)r {
    NSLog(@"Long press on view!");
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    //[super touchesEnded:touches withEvent:event];
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint endingPoint = [touch locationInView:self];
    
    if(self.hasStartingPoint && !self.isMoving) {
        self.hasStartingPoint = NO;
        
        // draw rect!
        /*
        
         */
        
        if((endingPoint.x - self.startingPoint.x) > 20.0F) { // HIGHLIHGT
            
            CGRect rect = CGRectMake(self.startingPoint.x,
                                     self.startingPoint.y,
                                     (endingPoint.x - self.startingPoint.x),
                                     40.0F);
            
            MADPView *view = [[MADPView alloc] initWithFrame:rect];
            view.backgroundColor = [UIColor yellowColor];
            view.alpha = 0.6;
            view.userInteractionEnabled = YES;
            view.guid = [self generateUUIDString];
            [self addSubview:view];
            
            if(self.drawBlock) {
                self.drawBlock(CGRectMake(self.startingPoint.x,
                                          self.startingPoint.y,
                                          (endingPoint.x - self.startingPoint.x),
                                          40.0F), view.guid);
            }
            
        } else { // TEXT

            CGRect rect = CGRectMake(self.startingPoint.x,
                                     self.startingPoint.y,
                                     150.0F,
                                     40.0F);
            
            MADPTextField *txtField = [[MADPTextField alloc] initWithFrame:rect];
            txtField.backgroundColor = [UIColor clearColor];
            txtField.textColor = [UIColor redColor];
            txtField.borderStyle = UITextBorderStyleNone;
            txtField.text = @"IMPORTANT!";
            txtField.enabled = NO;
            txtField.guid = [self generateUUIDString];
            [self addSubview:txtField];
            
            if(self.drawTextFieldBlock) {
                self.drawTextFieldBlock(txtField, txtField.guid);
            }
        }
        
    }
    
    self.isMoving = NO;
    self.movingView = nil;
}

@end
