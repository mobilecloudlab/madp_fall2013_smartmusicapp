//
//  MADPSessionSelectionViewController.m
//  MADP
//
//  Created by Kaarel Kruus on 27/10/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPSessionSelectionViewController.h"
#import "MADPScoreViewController.h"
#import "ServerBrowser.h"
#import "MADPSlaveSession.h"
#import "MADPPassCodeViewController.h"
#import "MBProgressHUD.h"
#import "MADPFileManager.h"
#import "AFNetworking.h"
#import "SSZipArchive.h"
#import "MADPView.h"
#import "MADPTextField.h"

@interface MADPSessionSelectionViewController ()

//@property (nonatomic, strong) ServerBrowser *serverBrowser;
@property (nonatomic, strong) NSMutableArray *availablePeers;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, strong) MADPScoreViewController *scoreController;

@property (nonatomic, strong) NSProgress *downloadProgress;

@end

@implementation MADPSessionSelectionViewController

- (id)init {
    self = [super initWithNibName:MADPNibName(@"MADPSessionSelectionViewController")
                           bundle:[NSBundle mainBundle]];
    if(self) {
        
        self.availablePeers = [NSMutableArray array];
        
    }
    return self;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Available sessions";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MCPeerID *peerID = [[MCPeerID alloc] initWithDisplayName:@"Bandsman"];
    self.session = [[MCSession alloc] initWithPeer:peerID];
    self.session.delegate = self;
    
    self.serviceBrowser = [[MCNearbyServiceBrowser alloc] initWithPeer:peerID serviceType:@"partiture"];
    self.serviceBrowser.delegate = self;
    [self.serviceBrowser startBrowsingForPeers];
}

- (void)browser:(MCNearbyServiceBrowser *)browser foundPeer:(MCPeerID *)peerID withDiscoveryInfo:(NSDictionary *)info {
    NSLog(@"- - - foundPeer (%@)", peerID.displayName);
    
    [self.availablePeers addObject:peerID];
    [self.dataTableView reloadData];
}

- (void)browser:(MCNearbyServiceBrowser *)browser lostPeer:(MCPeerID *)peerID {
    NSLog(@"- - - lostPeer (%@)", peerID.displayName);
    
    [self.availablePeers removeObject:peerID];
    [self.dataTableView reloadData];
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID {
    
    
    // draw!
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSDictionary *d = (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
        
       NSLog(@"- - - didReceiveData: %@", d);
        
        if([d objectForKey:@"textField"]) {
            
            NSLog(@"DRAW TEXT!");
            MADPTextField *textField = [d objectForKey:@"textField"];
            textField.guid = [d objectForKey:@"guid"];
            [self.scoreController.drawingView addSubview:textField];
            
        } else if([d objectForKey:@"rect"]) {
            
            NSString *rectString = [d objectForKey:@"rect"];
            CGRect rect = CGRectFromString(rectString);
            
            MADPView *view = [[MADPView alloc] initWithFrame:rect];
            view.backgroundColor = [UIColor redColor];
            view.alpha = 0.6F;
            view.guid = [d objectForKey:@"guid"];
            [self.scoreController.drawingView addSubview:view];
            
        } else if([d objectForKey:@"pageTurn"]) {
            
            int toPage = [[d objectForKey:@"pageTurn"] intValue];
            [self.scoreController.browser setCurrentPhotoIndex:toPage];
            
        } else if([d objectForKey:@"fileSetChecksum"] &&
                  [d objectForKey:@"fileSetHref"]) {
            
            //NSLog(@"BEFORE DOWNLOAD!");
            
            [self performSelectorOnMainThread:@selector(downloadIfNeeded:) withObject:d waitUntilDone:NO];
            
        } else if([d objectForKey:@"point"]) {
            
            NSString *pointString = [d objectForKey:@"point"];
            CGPoint point = CGPointFromString(pointString);
            
            UIView *view = [self.scoreController.drawingView viewForGuid:[d objectForKey:@"guid"]];
            
            CGRect frame = view.frame;
            frame.origin = point;
            
            view.frame = frame;
            
            NSLog(@"MOVE!");
            
        }
    });
}

- (void)downloadIfNeeded:(NSDictionary *)d {
    
    NSString *checksum = [d objectForKey:@"fileSetChecksum"];
    NSString *href = [d objectForKey:@"fileSetHref"];
    
    NSString *path = [[MADPFileManager sharedManager] fileSetsPath];
    NSString *filepath = [NSString stringWithFormat:@"%@/%@", path, checksum];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
        
        // we have it, open!
        self.scoreController = [[MADPScoreViewController alloc] initWithChecksum:checksum];
        //[self presentViewController:self.scoreController animated:YES completion:nil];
        [self.navigationController pushViewController:self.scoreController animated:YES];
        
    } else {
        
        NSLog(@"Download %@ at %@", checksum, href);
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        MADPFileManager *fileManager = [MADPFileManager sharedManager];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        
        NSURL *URL = [NSURL URLWithString:href];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        
        NSString *fileSetsDirPath = [fileManager fileSetsPath];
        NSString *zipFileName = [NSString stringWithFormat:@"%@.zip", checksum];
        
        NSString *zipFilePath = [fileSetsDirPath stringByAppendingFormat:@"/%@", zipFileName];
        NSString *unzipDirPath = [fileSetsDirPath stringByAppendingFormat:@"/%@", checksum];
        
        NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request
                                                                         progress:nil
                                                                      destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                                                                          return [NSURL fileURLWithPath:zipFilePath];
                                                                          
                                                                      } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                                                                          
                                                                          /* unzip */
                                                                          NSString *input = zipFilePath;
                                                                          NSString *output = unzipDirPath;
                                                                          
                                                                          NSError *err;
                                                                          [[NSFileManager defaultManager] createDirectoryAtPath:output
                                                                                                    withIntermediateDirectories:NO
                                                                                                                     attributes:nil
                                                                                                                          error:&err];
                                                                          
                                                                          [SSZipArchive unzipFileAtPath:input toDestination:output];
                                                                          
                                                                          /* delete zip file */
                                                                          [[[NSFileManager alloc] init] removeItemAtPath:input error:nil];
                                                                          
                                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                                              
                                                                              [hud hide:YES];
                                                                              
                                                                              self.scoreController = [[MADPScoreViewController alloc] initWithChecksum:checksum];
                                                                              //[self presentViewController:self.scoreController animated:YES completion:nil];
                                                                              [self.navigationController pushViewController:self.scoreController animated:YES];
                                                                              
                                                                          });
                                                                      }];
        
        [manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
            
            double d_percentage = ((double)totalBytesWritten / (double)totalBytesExpectedToWrite);
            int i_percentage = (int)(d_percentage * 100);
            
            hud.labelText = [NSString stringWithFormat:@"Downloading...%i%%", i_percentage];
            
        }];
        
        [downloadTask resume];
        
    }
    
}

- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID {
    NSLog(@"- - - didReceiveStream");
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress {
    NSLog(@"- - - didStartReceivingResourceWithName: %@", resourceName);
    
    //self.downloadProgress = progress;
    
    /*
    [progress addObserver:self
                forKeyPath:@"fractionCompleted"
                   options:NSKeyValueObservingOptionNew
                   context:NULL];
    */
    
}

/*
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if ([object isKindOfClass:[NSProgress class]]) {
        NSLog(@"Progress:...");
        return;
    }
    
    [super observeValueForKeyPath:keyPath
                         ofObject:object
                           change:change
                          context:context];
}
*/



- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error {
    NSLog(@"- - - didFinishReceivingResourceWithName: %@", resourceName);
    
    /*
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:localURL]];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    
    [self.view addSubview:imageView];
    */
}

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state {
    NSLog(@"- - - didChangeState: %i", state);
    
    if(state == MCSessionStateConnected) {
        
        /*
        self.hud.labelText = @"Downloading...";
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            //[NSThread sleepForTimeInterval:10.0F];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                self.scoreController = [[MADPScoreViewController alloc] init];
                //[self presentViewController:self.scoreController animated:YES completion:nil];
                [self.navigationController pushViewController:self.scoreController animated:YES];
            });
        });
         
         */
        
    } else {
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0F;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.availablePeers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = @"sessionListCell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    MCPeerID *peerID = [self.availablePeers objectAtIndex:indexPath.row];
    cell.textLabel.text = [peerID displayName];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSNetService *service = [self.serverBrowser.servers objectAtIndex:indexPath.row];
    //MADPSlaveSession *slaveSession = [[MADPSlaveSession alloc] initWithNetService:service];
    /*
    MADPScoreViewController *controller = [[MADPScoreViewController alloc] init];
    [self presentViewController:controller animated:YES completion:nil];
     */
    
    MADPPassCodeViewController *pcvc = [[MADPPassCodeViewController alloc] init];
    [pcvc setCompletionBlock:^(NSString *passCode) {
        
        MCPeerID *peerID = [self.availablePeers objectAtIndex:indexPath.row];
        
        // pass entered passcode as data
        NSData *context = [passCode dataUsingEncoding:NSUTF8StringEncoding];
        
        [self.serviceBrowser invitePeer:peerID toSession:self.session withContext:context timeout:5.0F];
        
        //self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        //self.hud.labelText = @"Connecting...";
    }];
    //[self presentViewController:pcvc animated:YES completion:nil];
    [self.navigationController pushViewController:pcvc animated:YES];
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.serverBrowser start];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.serverBrowser stop];
}

- (void)updateServerList {
    [self.dataTableView reloadData];
}

- (id)init {
    self = [super initWithNibName:MADPNibName(@"MADPSessionSelectionViewController")
                           bundle:[NSBundle mainBundle]];
    if(self) {
        self.serverBrowser = [[ServerBrowser alloc] init];
        self.serverBrowser.delegate = self;
    }
    return self;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0F;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.serverBrowser.servers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = @"sessionListCell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = @"Session";
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSNetService *service = [self.serverBrowser.servers objectAtIndex:indexPath.row];
    //MADPSlaveSession *slaveSession = [[MADPSlaveSession alloc] initWithNetService:service];
    
    MADPScoreViewController *controller = [[MADPScoreViewController alloc] init];
    [self presentViewController:controller animated:YES completion:nil];
}
*/

@end
