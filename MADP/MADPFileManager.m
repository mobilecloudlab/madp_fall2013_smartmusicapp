//
//  MADPFileManager.m
//  MADP
//
//  Created by Kaarel Kruus on 12/12/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import "MADPFileManager.h"
#import "MADPConstants.h"

@implementation MADPFileManager

static MADPFileManager *sharedManager = nil;

+ (id)sharedManager {
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedManager = [[MADPFileManager alloc] init];
    });
    return sharedManager;
}

- (id)init {
    self = [super init];
    if(self) {
        
        /* create storage dir for filesets (if needed) */
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSString *fileSetsPath = [self fileSetsPath];
        
        BOOL isDirectory = YES;
        
        if(![fileManager fileExistsAtPath:fileSetsPath isDirectory:&isDirectory]) {
            [fileManager createDirectoryAtPath:fileSetsPath
                   withIntermediateDirectories:YES
                                    attributes:Nil
                                         error:nil];
            //NSLog(@"- - - CREATE FILESET DIR!");
        } else {
            //NSLog(@"- - - FILESET DIR EXISTS!");
        }
    }
    return self;
}

- (NSString *)fileSetsPath {
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    return [documentsPath stringByAppendingString:cFileSetsDirPath];
}

- (NSArray *)filePathsForChecksum:(NSString *)checksum {
    NSString *dirPath = [NSString stringWithFormat:@"%@/%@/", [self fileSetsPath], checksum];
    NSArray *filePaths = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dirPath error:nil];
    NSPredicate *fltr = [NSPredicate predicateWithFormat:@"self ENDSWITH '.png'"];
    NSArray *onlyPNGs = [filePaths filteredArrayUsingPredicate:fltr];
    return onlyPNGs;
}

@end
