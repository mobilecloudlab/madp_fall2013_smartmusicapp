//
//  MADPPassCodeViewController.h
//  MADP
//
//  Created by Kaarel Kruus on 24/11/13.
//  Copyright (c) 2013 Kaarel Kruus. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ResponseBlock)(NSString *passCode);

@interface MADPPassCodeViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITextField *passCodeTextField;
@property (nonatomic, copy) ResponseBlock completionBlock;

- (IBAction)onEnter:(id)sender;

@end
