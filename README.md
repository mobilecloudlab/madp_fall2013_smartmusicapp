# Partiture

An interoperable music stand application for mobile devices.

## Preconditions / requirements

* The project is built in Xcode 5
* The application requires iOS7 as the operating system

## Additional information

All external frameworks are included in the project, so no dependency issues should arise

## Download

```
$ git clone https://kaarelkruus@bitbucket.org/mobilecloudlab/madp_fall2013_smartmusicapp.git
```

## Setup

1. Download the project
2. Open the project in Xcode 5
3. Under the MADP project / target Build settings, set the code signing identities to run the app on a device
4. Build it, run it & have fun!

